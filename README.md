# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

![GirlAnimation](./public/girlanimation.JPG)

## Steps
- Go to GitLab and create a new project and copy Clone with HTTPS to clipboard.
- Go to Terminal > cd projects > git clone https://gitlab.com/athorndia/robotanimation.git > cd robotanimations > code .
- Take a moment to admire empty VS Code
- Go back to terminal and run the following code:
    - Create vite application:
        ```
        npm create vite@latest ./ -- --template react
        ```
    - Install other dependencies:
        ```
        npm install --legacy-peer-deps three @react-three/fiber @react-three/drei maath react-tilt react-vertical-timeline-component @emailjs/browser framer-motion react-router-dom
        ```
        - Please note you only need to install @react-three/fiber and @react-three/drei... the rest were added for more play practice. 
        - Maybe the --legacy-peer-deps is not needed... it was advised in a different three.js tutorial. 
- Go to VS Code and get rid of the following boilerplate code:
    - Delete src/index.css and remove the import './index.css' from src/main.jsx.
    - Remove all imports from App.jsx (i.e., import { useState } from 'react', import reactLogo from './assets/react.svg', import viteLogo from '/vite.svg', and import './App.css') and delete everything within the return section.

- Add stuff
    - Go to src/App.jsx and import Canvas 
        - add Canvas components to return section.
        - add other stuff

- Install Mantine
    - Enter the following code in Terminal:
        ```
        npm install @mantine/core @mantine/hooks
        npm install --save-dev postcss postcss-preset-mantine postcss-simple-vars
        ```
    - Add Mantine elements

- Go to Mixamo > select Character > select Animations > download FBX > extract > open Blender > new project > delete default scene > import > select largest file from download > check that Armature/Automatic Bone Orientation is selected.
- Import an animation file > check that Armature/Automatic Bone Orientation is selected > got to Armature.001 and change Animation node name (keyframe symbol) > switch to Animation workspace > select node name and click > check that animation is attached to model > export as gltf/glb > run this code in Terminal:
    ```
    npx gltfjsx public/girl.glb -t
    or 
    npx gltfjsx public/girl.gltf
    ```
- Take a look at the repo files for additional code changes... many random things were done and redone prn.
- Unable to add another 3D object(s) to create a background (I gave up after many attempts), so I snagged a mp4 video and made it the background.
- You can move and pan the 3D girl object.

## Next Steps
- Add more animations to Girl.
- Practice using Blender more. 
- Watch more tutorials to get better at implementing 3D objects using Vite, React Drei and React Fiber.

## References / Materials / Tutorials etc.
- [Three.JS](https://threejs.org/)
- [Mantine.dev](https://mantine.dev/)
- [Pmndrs.docs](https://docs.pmnd.rs/react-three-fiber/getting-started/introduction)
- [Mixamo](https://www.mixamo.com/#/)
- [Pixabay](https://pixabay.com/videos/sunset-blender-mountains-river-3d-65182/)
- [Youtube - Code WorkShop - Loading Animated Characters in React Three Fiber](https://www.youtube.com/watch?v=q7yH_ajINpA)
- [Youtube - Wawa Sensei - React Three Fiber Tutorial - How to animate 3D models](https://www.youtube.com/watch?v=mdj7Z3PCxRg)

## Thanks for checking this mini project out! ❤️