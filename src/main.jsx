import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import '@mantine/core/styles.css';
import { MantineProvider, createTheme } from '@mantine/core';

const theme = createTheme({
  /** Put your mantine theme override here */
  globalStyles: () => ({
    body: {
      width: "100vw",
      height: "100vh",
    },
    "#root": {
      width: "100%",
      height: "100%",
    },
  }),
});


ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <MantineProvider theme={theme}>
        <App />
    </MantineProvider>
  </React.StrictMode>
);
