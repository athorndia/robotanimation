import React, { Suspense } from "react";
import { Canvas } from "@react-three/fiber";
import Girl from "./components/Girl.jsx";
import { OrbitControls } from "@react-three/drei";
import "./App.css"; 

function App() {
  return (
    <>
      <div
        className="background"
        style={{ position: "relative", overflow: "hidden", display: "flex", justifyContent: "center", alignItems: "center", height: "100vh" }}
      >
        <video autoPlay loop muted style={{ position: "absolute", width: "100%", height: "100%", objectFit: "cover" }}>
          <source src="/sunset.mp4" type="video/mp4" />
        </video>
        <Canvas 
          camera={{ position: [1, 1.5, 2.5], fov: 50 }} 
          shadows
          frameloop='demand'
          dpr={[1, 2]}
          gl={{ preserveDrawingBuffer: true }}
        >
          <OrbitControls 
            autoRotate
            enableZoom={false}
            maxPolarAngle={Math.PI / 2}
            minPolarAngle={Math.PI / 2}
          />
          <directionalLight intensity={0.5} />
          <ambientLight intensity={0.6} />
          <Suspense fallback={null}>
            <Girl />
          </Suspense>
        </Canvas>
      </div>
    </>
  )
}

export default App;
